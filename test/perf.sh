#!/bin/bash
# apt-get update
# apt-get install openjdk-11-jdk -y
# apt-get install git -y
# apt-get install curl -y
# git --version
# java -version
mkdir qe-test-results
cd /opt
# curl -O https://archive.apache.org/dist/jmeter/binaries/apache-jmeter-5.4.3.tgz
# tar -xvf apache-jmeter-5.4.3.tgz
# rm -r apache-jmeter-5.4.3.tgz
cd -
pwd
mkdir temp
ls temp
# /opt/apache-jmeter-5.4.3/bin/jmeter -n -t test/simple.jmx -l temp/simple_1.jtl -e -o temp/simple_1
python ./test/parseError.py ./test/statistics.json
retVal=$?
if [ $retVal -ne 0 ]; then
    echo "Perfomance test FAILED"
fi
exit $retVal
