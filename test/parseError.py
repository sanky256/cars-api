import json
import sys

path=sys.argv

def parseForError(file):
    try:
        f = open(file)
    except FileNotFoundError:
        print("No file in report to parse")
    try:
        data=json.load(f)
        dic = data['Total']
        dic2=dic['errorPct']
    except parseForError(f):
        print("No errorPct in the report")
    if dic2>2:
        print("Performance test total Error rate exceeds 2%")
        f.close()
        sys.exit(0)
    else:
        print("Performance test total Error rate:" , dic2)
        f.close()
        sys.exit(0)
parseForError(sys.argv[1])
