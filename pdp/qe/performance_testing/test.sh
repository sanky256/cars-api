#!/bin/bash
TC1="CAS_CCDS_1.5_PPE_p1_LearningContext"
TC2="CAS_CCDS_1.5_PPE_p2_CourseSettings"
TC3="CAS_CCDS_1.5_PPE_p3_TOC"

apt-get update
apt-get install openjdk-11-jdk -y

python --version
mkdir qe-test-results
# curl -O https://archive.apache.org/dist/jmeter/binaries/apache-jmeter-5.4.3.tgz
# tar -xvf apache-jmeter-5.4.3.tgz
# rm -r apache-jmeter-5.4.3.tgz
echo $PATH
mkdir temp
pwd


git clone https://oauth2:glpat-qVihdevZWtHxkz2rnik1@gitlab.com/pearsontechnology/gpt/cls-domain-services/cls/cas/cas-automation-test.git
cd cas-automation-test
git checkout main
cd ..

echo Starting ${TC1} script
# apache-jmeter-5.4.3/bin/jmeter -n -t cas-automation-test/src/test/resources/jmeterPerformanceScripts/CCDS/${TC1}.jmx -l temp/${TC1}.jtl -e -o temp/${TC1}

# python pdp/qe/performance_testing/parseError.py temp/${TC1}/statistics.json
retVal1=0


echo Starting ${TC2} script
# apache-jmeter-5.4.3/bin/jmeter -n -t cas-automation-test/src/test/resources/jmeterPerformanceScripts/CCDS/${TC2}.jmx -l temp/${TC2}.jtl -e -o temp/${TC2}
# python pdp/qe/performance_testing/parseError.py temp/"${TC2}"/statistics.json
retVal2=0


echo Starting ${TC3} script
# apache-jmeter-5.4.3/bin/jmeter -n -t cas-automation-test/src/test/resources/jmeterPerformanceScripts/CCDS/${TC3}.jmx -l temp/${TC3}.jtl -e -o temp/${TC3}
# python pdp/qe/performance_testing/parseError.py temp/${TC3}/statistics.json
retVal3=0

cp -rf temp/* qe-test-results
ls qe-test-results

if [ $retVal1 -ne 0 ] || [ $retVal2 -ne 0 ] || [ $retVal3 -ne 0 ] ; then
    echo "Perfomance test FAILED"
    exit 1
else exit 0    
fi
